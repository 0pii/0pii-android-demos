package com.zeropii.hackalert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("DefaultLocale")
public class HackAlertActivity extends Activity {

  protected static final String TAG = "HackAlertActivity";
  
  private final int LIGHT_BLUE = Color.rgb(173, 216, 230);
  
  private final int DARK_GREEN = Color.rgb(34, 139, 34);
  
  private CheckEmailManager checkEmailManager = CheckEmailManager.getInstance();
  
  private final HackAlertActivity context = this;
    
  // cache of hacked emails repository
  private Map<String, Set<String>> emailsRepoCache = new HashMap<String, Set<String>>();
  
  public static enum MailServer {
	  //we can add more domains in the future and contacts will be filtered by these domains
	  GMAIL("gmail.com"), YAHOO("yahoo.com"), LIVE("live.com"), HOTMAIL("hotmail.com"), AOL("aol.com"), YANDEX("yandex.ru");
	  
	  private String name;
	  
	  MailServer(String name) {
		  this.name = name;
	  }
	  public String getName(){
		  return name;
	  }
  }
  
  private EditText editTextEmail;
  private TextView textViewStatusDatabase;
  private TextView textViewResult;
  
  private TextView textViewEmailResult;
  private TextView textViewContactsProgress;
  private TextView textViewContactsResult;
  private TableLayout tableCommonEmailServers;
  
  private ContactsProgressBar progressBarContacts;
  private LinearLayout progressBarLayout;
  private LinearLayout approveCheckLayout;
  private TableLayout hackedContactsLayout;
  private Button buttonApproveCheck;
  private Button buttonShare;

  
  private Set<String> downloadStarted = new HashSet<String>();
  
  private Map<String, EmailGroup> contactsEmails = new HashMap<String, EmailGroup>();
  
  private List<ContactDetail> hackedContacts = Collections.synchronizedList(new ArrayList<HackAlertActivity.ContactDetail>());
  
  /**
   * store contact details
   * @author flado
   *
   */
  class ContactDetail {
	  
	  	private String email;
	  	private String name;
	  	private boolean displayed = false;
  
		public ContactDetail(String email, String name) {			
			this.email = email;
			this.name = name;	  
		}
	
		public boolean isDisplayed() {
			return displayed;
		}
		
		public void setDisplayed(boolean displayed) {
			this.displayed = displayed;
		}
		
		public String getEmail() {
			return email;
		}
		
		public String getName() {
			return name;
		}	  
  }
  
  
  /**
   * AsyncTask for checking all contacts that have emails on any of the predefined MailServer values
   * @author yfxa
   *
   */
  private class CheckContactsTask extends AsyncTask<Void, Integer, List<ContactDetail>> {
	
	protected static final String TAG = "CheckContactsTask";
	private int totalEmails;
	final Button buttonSelectAll = new Button(context);
	final Button buttonSendEmail = new Button(context);
		  
	@Override
	protected void onPreExecute() {
		contactsEmails = getEmaiGroups();
		Log.d(TAG, "CheckContacts: " + contactsEmails.size());

		totalEmails = 0;
		
		Iterator<String> it = contactsEmails.keySet().iterator();
	    while (it.hasNext()) {      
	      final String key = it.next();
	      totalEmails += contactsEmails.get(key).emails.size();
	    }
		textViewContactsProgress.setText("Checking " + totalEmails + " emails from your contacts ... please wait");
		//send email button				
		buttonSendEmail.setText("Send Email");				
		buttonSendEmail.setTypeface(null, Typeface.BOLD);
		buttonSendEmail.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
		buttonSendEmail.setOnClickListener(new Button.OnClickListener() {		
				@Override
				public void onClick(View v) {
					List<String> selected = getSelectedEmails();
					// TODO(edisonn) disable send mail also! 
					if (selected.size() == 0) {
					  return;
					}
					String to = selected.remove(0);
					StringBuilder bcc = new StringBuilder();
					
					for(String em: selected) {
						bcc.append(em).append(";");	
					}						
					
					Uri mailUri = Uri.parse("mailto:" + to + (bcc.length()>0 ? "?bcc=" + bcc.toString() : ""));					
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, mailUri);				
					emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "About your email"); 
					emailIntent.putExtra(
			             Intent.EXTRA_TEXT,
			             Html.fromHtml(new StringBuilder()
			                 .append("<p><b>Hi there,</b><br><br>I was informed that your email account might have been hacked. " +
			                 		"You can check for yourself using the <a href=\"http://play.google.com/store/apps/details?id=com.zeropii.hackalert\">HackAlert</a> application available on <a href=\"http://play.google.com/store/apps/details?id=com.zeropii.hackalert\">Play Store</a>. " +							                 		
			                 		"For more details you can read the <a href=\"http://www.0pii.com/5-million-emails-hacked-dont-panic-dont-verify-if-your-e-mail-is-leaked-on-potentially-dubious-websites/\">0PII blog</a>." +
			                 		"</p>")
			                 .append("<br><br> All you need to do is to <b>change your email password ASAP</b>.<br><br>")
			                 .append("<small><p><i>Please note that 0PII does not know any of your passwords. Visit <a href=\"http://www.0PII.com\">http://www.0PII.com</a> for more information</i></p></small>")
			                 .toString())
					);													
				    emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
					try {
						startActivity(Intent.createChooser(emailIntent, "Sending mail..."));
				    } catch (android.content.ActivityNotFoundException ex) {
				        Toast.makeText(context, "There are no mail clients installed", Toast.LENGTH_SHORT).show();
				    }
				}
		});
		buttonSendEmail.setVisibility(View.GONE);
		hackedContactsLayout.addView(buttonSendEmail);
		
		//select All button		
		buttonSelectAll.setText("Discard All");
		TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
		lp.setMargins(0, 40, 0, 5);
		buttonSelectAll.setLayoutParams(lp);
		buttonSelectAll.setOnClickListener(new Button.OnClickListener() {		
				@Override
				public void onClick(View v) {
					TableRow row;
					boolean selectAll = true;
					if (buttonSelectAll.getText().toString().contains("Select")) {
						buttonSelectAll.setText("Discard All");
						selectAll = true;
						
					} else {
						buttonSelectAll.setText("Select All");
						selectAll = false;								
					}
					ContactCheckBox cb;
					for(int i=0; i < hackedContactsLayout.getChildCount(); i++){
						if (hackedContactsLayout.getChildAt(i) instanceof TableRow) {
							row = ((TableRow)hackedContactsLayout.getChildAt(i));
							cb = (ContactCheckBox)row.getChildAt(0);
							cb.setChecked(selectAll);
							if (cb.isChecked()) {
								row.setBackgroundColor(LIGHT_BLUE);
							} else {
								row.setBackgroundColor(Color.WHITE);
							}									
						}
					}
				
					
				}
		});
		buttonSelectAll.setVisibility(View.GONE);
		hackedContactsLayout.addView(buttonSelectAll);
		//
	}
	
	
	@Override
	protected void onProgressUpdate(Integer... progress) {
		//setProgressPercent
		Log.d(TAG, ">> progress:" + progress[0]);
		progressBarContacts.setProgress(progress[0]);
		
		//check progress
		if (progress[0] == 100) {			
			progressBarLayout.setVisibility(View.GONE);
			
			buttonSendEmail.setVisibility(View.VISIBLE);
			buttonSelectAll.setVisibility(View.VISIBLE);
			
			textViewContactsResult.setPadding(5, 5, 5, 5);					
			if (hackedContacts.size() > 0) {
				textViewContactsResult.setBackgroundColor(LIGHT_BLUE);	
				textViewContactsResult.setTextColor(Color.RED);				
				textViewContactsResult.setText(hackedContacts.size() + " emails of your contacts have been hacked. " +
						"You can help them recover their privacy by sending an email now! You can individualy select the contact you want to inform.");				
			} else {
				textViewContactsResult.setTextColor(DARK_GREEN);
				textViewContactsResult.setText("None of your contacts email was hacked !");
			}						
		} else {
			progressBarContacts.setText(progress[1] + "/" + progress[2] + " contacts");
		}
		//////////
		TableRow contactRow = null;
		TextView contactText = null;
		ContactDetail[] cdArray = hackedContacts.toArray(new ContactDetail[hackedContacts.size()]);
		for(ContactDetail contact: cdArray) {
			if (!contact.isDisplayed()) {
				TableLayout.LayoutParams td_tr = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);						
				//create checkbox
				final ContactCheckBox contactCheckbox = new ContactCheckBox(context);
				contactCheckbox.setEmail(contact.getEmail());
				contactCheckbox.setPadding(5, 5, 5, 5);					
				contactCheckbox.setChecked(true);
				contactCheckbox.setLayoutParams(new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));					
				contactCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if (isChecked) {								
							((TableRow)contactCheckbox.getParent()).setBackgroundColor(Color.rgb(173, 216, 230));
						} else {
							((TableRow)contactCheckbox.getParent()).setBackgroundColor(Color.WHITE);
						}						
					}
				});
								
				//create text
				contactText = new TextView(context);
				contactText.setPadding(5, 5, 5, 10);
				contactText.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1));
				contactText.setGravity(Gravity.LEFT);
				
				if (contact.getEmail().equals(contact.getName())) {
					contactText.setText(Html.fromHtml("<b>" + contact.getEmail() + "</b>"));
				} else {
					contactText.setText(Html.fromHtml("<b>" + contact.getName() + "</b>" +  "<br />" + "<small>" + contact.getEmail() + "</small>"));
				}
				
				//create table row
				contactRow = new TableRow(context);
				contactRow.setLayoutParams(td_tr);
				contactRow.setPadding(5, 10, 5, 10);
				//add contact to row
				contactRow.setBackgroundColor(LIGHT_BLUE);
				contactRow.addView(contactCheckbox);
				contactRow.addView(contactText);
				
				contactRow.setOnClickListener(new AdapterView.OnClickListener() {						
					@Override
					public void onClick(View v) {
						CheckBox cb = (CheckBox)((TableRow)v).getChildAt(0);
						cb.setChecked(!cb.isChecked());
						if (cb.isChecked()) {
							((TableRow)v).setBackgroundColor(LIGHT_BLUE);
						} else {
							((TableRow)v).setBackgroundColor(Color.WHITE);
						}
					}
				});
				
				View separator = new View(context);
	            separator.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
	            separator.setBackgroundColor(Color.LTGRAY);		            
	            //add row to table
				hackedContactsLayout.addView(contactRow);
				//add separator to table
				hackedContactsLayout.addView(separator);
				//update contact display flag
				contact.setDisplayed(true);
			}
		}//for
	}
	  
	@Override
	protected List<ContactDetail> doInBackground(Void... params) {
		final AtomicInteger taskCounter = new AtomicInteger(0);
		Iterator<String> it = contactsEmails.keySet().iterator();
		while (it.hasNext()) {			
			final String key = it.next();
			
//      hackedContacts.put(email, contactsEmails.get(email));

			//check if developer mode and mock it so that don't make real calls 
			if (com.zeropii.flags.Constants.DEVELOPER_MODE) {						
					try {
						Thread.sleep(100);
						EmailGroup eg = contactsEmails.get(key);
						//
						for (int index = 0; index < eg.emails.size(); index++) {
							String email = eg.emails.get(index);
						  	String name = eg.names.get(index);
						  
  							Log.d(TAG, email + " [ DEMO HACKED ] - DEVELOPER MODE ");  							
  							hackedContacts.add(new ContactDetail(email, name));
  							int i = taskCounter.incrementAndGet();
  							publishProgress((int) ((i / (float) totalEmails) * 100), i, totalEmails);
						}
					} catch (InterruptedException e) {}
				
			} else {		
			  
				DownloadTask task = new DownloadTask(key, taskCounter, new DownloadTask.ValidateEmailCallback() {
					private EmailGroup grp = contactsEmails.get(key);
					
					@Override
					public void hacked(int index) {							
						Log.d(TAG, grp.emails.get(index) + " [ HACKED ]");						
						hackedContacts.add(new ContactDetail(grp.emails.get(index), grp.names.get(index)));
					}
					
					@Override
					public EmailGroup getEmailGroup() {
						return this.grp;
					}

					@Override
					public void done() {
						//task has finished -> update progress
						publishProgress((int) ((taskCounter.get() / (float) totalEmails) * 100), taskCounter.get(), totalEmails);							
					}
					
				});
				
				checkEmailManager.execute(task);
			}
		} //while			
		
		// do not accept more tasks
		checkEmailManager.shutdown();		
		//wait to finish until all tasks are finished (using specified timeout)			
		checkEmailManager.awaitTermination(300); // block until all tasks finish execution (wait max. 5 min)		
		Log.d(TAG, "### Hacked emails: " + hackedContacts.size());
		return hackedContacts;		
	}
	  
  }
 	
  // all emails that start with the same key
  class EmailGroup {
    final String key;
    List<String> emails = new ArrayList<String>();
    List<String> names = new ArrayList<String>();
    EmailGroup(String key) {
      this.key = key;
    }
    
    synchronized void add(String email, String name) {
      emails.add(email);
      names.add(name);
    }
  }
  
  /**
   * Get list of e-mails from all local contacts
   *  
   * @return Map<K, V> where K = first 2 characters from email, V = EmailGroup (list of emails & names with same key)
   */
  private Map<String, EmailGroup> getEmaiGroups() {
	    Map<String, String> emlRecs = new HashMap<String, String>();	    	   
	    Map<String, EmailGroup> emlGrp = new HashMap<String, EmailGroup>();   
	    
	    ContentResolver cr = this.getContentResolver();
	    String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID, 
	            ContactsContract.Contacts.DISPLAY_NAME,	            
	            ContactsContract.CommonDataKinds.Email.DATA};
	    String order = "CASE WHEN " 
	            + ContactsContract.Contacts.DISPLAY_NAME 
	            + " NOT LIKE '%@%' THEN 1 ELSE 2 END, " 
	            + ContactsContract.Contacts.DISPLAY_NAME 
	            + ", " 
	            + ContactsContract.CommonDataKinds.Email.DATA
	            + " COLLATE NOCASE";
	    StringBuilder filter = new StringBuilder();
	    for(MailServer s: MailServer.values()) {	    	
	    	filter.append(ContactsContract.CommonDataKinds.Email.DATA).append(" LIKE '%@").append(s.getName()).append("'").append(" OR ");
	    }
	    filter.delete(filter.length()-4, filter.length()); //delete last " OR " 
	    
	    Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter.toString(), null, order);
	    if (cur.moveToFirst()) {
	        do {
	            //names comes in hand sometimes
	            String name = cur.getString(1);
	            String emlAddr = cur.getString(2);
	            //keep unique only
	            if (!emlRecs.containsKey(emlAddr)) {
	            	emlRecs.put(emlAddr, name);
	            	
	            	String canonicalEmail = emlAddr; // todo
	            	String key = canonicalEmail.substring(0, 2).toLowerCase();
	            	EmailGroup grp = emlGrp.get(key);
	            	if (grp == null) {
	            	  grp = new EmailGroup(key);
	            	  emlGrp.put(key, grp);
	            	}
	            	grp.add(canonicalEmail, name);
	            	
	            }	            
	        } while (cur.moveToNext());
	    }

	    cur.close();
	    return emlGrp;
	}
  
  /**
   * clear focus on user name field and hide keyboard
   * @param v
   */
  private void hideKeyboard(View v) {
	  editTextEmail.clearFocus();
	  InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE); 
      imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
  }
  
  /**
   * get all selected emails
   * @return
   */
  private List<String> getSelectedEmails() {
	int c = hackedContactsLayout.getChildCount();
	TableRow row;
	ContactCheckBox cb;
	List<String> emails = new ArrayList<String>();
	for(int i=0; i < c; i++){
		if (hackedContactsLayout.getChildAt(i) instanceof TableRow) {
			row = ((TableRow)hackedContactsLayout.getChildAt(i));
			cb = (ContactCheckBox)row.getChildAt(0);
			if (cb.isChecked()) {
				emails.add(cb.getEmail());
			}
		}
	}
	return emails;
  }
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //hide keyboard when starts
    this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    
    requestWindowFeature(Window.FEATURE_NO_TITLE);

    setContentView(R.layout.activity_hack_alert);
    
    editTextEmail = (EditText) findViewById(R.id.editTextEmail);
    
    
    textViewStatusDatabase = (TextView) findViewById(R.id.textViewStatusDatabase);
    textViewResult = (TextView) findViewById(R.id.textViewResult);
    textViewResult.setVisibility(View.GONE);
    
    textViewContactsResult = (TextView) findViewById(R.id.textViewContactsResult);
    
    textViewEmailResult = (TextView) findViewById(R.id.textViewEmailResult);
    tableCommonEmailServers = (TableLayout) findViewById(R.id.tableCommonEmailServers);
    
    progressBarContacts = (ContactsProgressBar) findViewById(R.id.contactsProgressBar);
    progressBarContacts.setTextSize(30);
    
    textViewContactsProgress = (TextView) findViewById(R.id.textViewContactsProgress);
    progressBarLayout = (LinearLayout) findViewById(R.id.progressBarLayout);
    progressBarLayout.setVisibility(View.GONE);
    
    approveCheckLayout = (LinearLayout) findViewById(R.id.approveCheckLayout);
    buttonApproveCheck = (Button) findViewById(R.id.buttonApproveCheck);
    buttonShare = (Button) findViewById(R.id.buttonShare);
    
    buttonShare.setOnClickListener(new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);			
			shareIntent.putExtra(Intent.EXTRA_TEXT, "Check out this app: http://play.google.com/store/apps/details?id=com.zeropii.hackalert");
			shareIntent.setType("text/plain");
			//
			try {
				startActivity(Intent.createChooser(shareIntent, "Share HackAlert app ..."));
		    } catch (android.content.ActivityNotFoundException ex) {
		        Toast.makeText(context, "There are no sharing apps installed", Toast.LENGTH_SHORT).show();
		    }
		}
    });
    
    
    hackedContactsLayout = (TableLayout) findViewById(R.id.hackedContactsLayout);    
    
    editTextEmail.addTextChangedListener(new TextWatcher() {
      
      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {    	  
    	  updateStatusDatabase();
    	  updateResult();
      }
      
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }
      
      @Override
      public void afterTextChanged(Editable s) {
      }
    });
    
    updateStatusDatabase();
    
    buttonApproveCheck.setOnClickListener(new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			approveCheckLayout.setVisibility(View.GONE);
			progressBarLayout.setVisibility(View.VISIBLE);
			//check contacts
		    new CheckContactsTask().execute();
		}
    });
    
    
  } //onCreate

  private void updateStatusDatabase() {
    runOnUiThread(new Runnable() {
      
      @Override
      public void run() {
        String msg = " Please enter at least 2 characters.";
        String email = editTextEmail.getText().toString();
        if (email.length() >= 2) {
          // IMPORTANT we only disclose to the server the first 2 letters of the email we test.
          // The web service will return all emails that start with those 2 letters.
          // In this way limit the amout of data we download from 50MB to worst case of 1MB
          // and the datbase is downloaded way before the user finishes to introduce the email
          // TODO(edison.nica): instead of grouping emails by the first two letters, we can improve privacy
          // by hashing the email address so we have high colision rates - like 1000-10000 emails per unique hash
          final String key = email.substring(0, 2).toLowerCase();
          if (emailsRepoCache.containsKey(key)) {
            // Empty looks better
            msg = ""; //" " + key.toUpperCase() + " Database downloaded.";
          } else {
            msg = " Downloading Database of emails starting with " + key.toUpperCase() + "...";
            
            if (!downloadStarted.contains(key)) {
              downloadStarted.add(key);
              DownloadTask task = new DownloadTask(key, new DownloadTask.DownloadCallback() {				
				@Override
				public void done(Set<String> emails) {
					Log.d(TAG, "callback.done() ");
					emailsRepoCache.put(key, emails);
					runOnUiThread(new Runnable() {
		                @Override
		                public void run() {
		                  // Empty looks better
		                  String msg = ""; //" " + key.toUpperCase() + " Database downloaded.";
		                  textViewStatusDatabase.setText(msg);
		                  updateResult();
		                }
		              });			
				}
              });
              Thread th = new Thread(task);
              th.start();
            }
          }
        }
        textViewStatusDatabase.setText(msg);
      }
    });    
  }
  
  /**
   * Check if email is hacked and display message
   */
  private void updateResult() {
    runOnUiThread(new Runnable() {
      
      @Override
      public void run() {
        String userOrEmail = editTextEmail.getText().toString();
        
        textViewResult.setText("");
        textViewResult.setVisibility(View.GONE);
        
        // TODO(edisonn): technically, there are email servers that could accept @ in their username.
        if (userOrEmail.contains("@")) {
          updateResult(DownloadTask.canonicalEmail(userOrEmail), -1);
        } else {
          for (int i = 0 ; i < tableCommonEmailServers.getChildCount(); i++) {
            // TODO(edisonn): checks, constants
            TableRow tr = (TableRow)tableCommonEmailServers.getChildAt(i);
            TextView hostTextView = (TextView) tr.getChildAt(0);
            updateResult(DownloadTask.canonicalEmail(userOrEmail, hostTextView.getText().toString()), i);
          }
        }        
      }

    });
    
  }


  private void updateResult(String email, int tableRow) {
    if (email == null || email.length() == 0) {
      // ...
    }
    
    String msg = ""; 
    int color = 0;
    boolean hacked = false;

    if (email.length() >= 2) {
      String key = email.substring(0, 2).toLowerCase();
      if (emailsRepoCache.containsKey(key)) {
        Set<String> emails_subset = emailsRepoCache.get(key);
        if (emails_subset.contains(email.toString().toLowerCase())) {
          msg = "[ HACKED ]";             
          color = 0xffff0000;
          hacked = true;
        } else {
          msg = "[ safe ]";
          color = 0xff008000;
        }
        if (tableRow < 0) {
          msg = email + " " + msg;
        }
      }
    }
    
    // TODO(edisonn): constants
    TextView serverResult;
    if (tableRow < 0) {
      /*serverResult = new TextView(context);      
      serverResult.setLayoutParams(new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
      serverResult.setPadding(20, 0, 0, 0);
      serverResult.setTextAppearance(context, android.R.attr.textAppearanceMedium);
      */
      serverResult = textViewEmailResult;
      serverResult.setVisibility(View.VISIBLE);
      tableCommonEmailServers.setVisibility(View.GONE);
    } else {
      TableRow tr = (TableRow)tableCommonEmailServers.getChildAt(tableRow);
      serverResult = (TextView) tr.getChildAt(1);
      
      textViewEmailResult.setVisibility(View.GONE);
      tableCommonEmailServers.setVisibility(View.VISIBLE);
    }
    
    serverResult.setText(msg);
    serverResult.setTextColor(color);
    
    if (hacked) {
    	textViewResult.setVisibility(View.VISIBLE);
    	textViewResult.setText("Change passwords if you have not changed them in a while. Do not reuse old passwords! (We do not have access to what password was stolen, just use new ones)!");
    	textViewResult.setTextColor(color);
    	//hide keyboard so that user can see the long message.
    	hideKeyboard(editTextEmail);
    }
  }
  
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.hack_alert, menu);
    return true;
  }

 
  
}
