package com.zeropii.hackalert;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.zeropii.hackalert.HackAlertActivity.EmailGroup;

import android.annotation.SuppressLint;
import android.util.Log;

/**
 * 
 * @author flado
 * 
 */
@SuppressLint("DefaultLocale")
public final class DownloadTask implements Runnable {

	private static final String TAG = "DownloadTask";

	private static final String WS_BASE_URL = "http://alert.0pii.com/h/";

	private String key;
	
	private AtomicInteger counter; 
	
	private DownloadCallback downloadCallback = null;
	private ValidateEmailCallback validateCallback = null;

	public interface DownloadCallback {
		public void done(Set<String> emails);		
	}
	
	public interface ValidateEmailCallback {
		public EmailGroup getEmailGroup();
		public void hacked(int index);
		public void done();
	}

	public DownloadTask(final String key, DownloadCallback cb) {
		this.key = key;		
		this.downloadCallback = cb;
		
	}
	
	public DownloadTask(final String key) {
		this.key = key;		
	}
	
	public DownloadTask(final String key, AtomicInteger counter, ValidateEmailCallback vc) {
		this.key = key;
		this.validateCallback = vc;
		this.counter = counter;
	}
	
	public void run() {
		Set<String> emails = new HashSet<String>();
	  
		String httpParam = String.format("%02x%02x", (int) key.charAt(0), (int) key.charAt(1));
		final String url = WS_BASE_URL + httpParam;

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try {
				response = httpclient.execute(new HttpGet(url));
				StatusLine statusLine = response.getStatusLine();
				if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
					
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);					
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(out.toByteArray())));
					
					String line = null;
					String host = null;
					int count = 0;	
					EmailGroup grp = null;
					if (validateCallback != null) {
						grp = validateCallback.getEmailGroup();
					}
					
					while ((line = bufferedReader.readLine()) != null) {
						count++;						
						if (line.length() > 0 && line.charAt(0) == '@') {
							host = line.trim();
						} else {							
							String email1 = canonicalEmail(key + line.trim(), host).toLowerCase();
						  
						    if (downloadCallback != null) {           
						      emails.add(email1);
						    }						  
						  
							if (validateCallback != null) {
							  for (int i = 0 ; i < grp.emails.size(); i++) {
							    String email = canonicalEmail(grp.emails.get(i));
							    if (email1.equalsIgnoreCase(email)) {
							      validateCallback.hacked(i);
							    }
							  }            
							}
						}//else @
					} //while
					
					out.close();
					Log.d(TAG, url  + " # " + count + " lines");
					if (validateCallback != null) {
						counter.addAndGet(grp.emails.size());
						Log.d(TAG, "## task has finished to check: " + counter.get() + " emails");
						validateCallback.done();
					}
					
				} else {
					// Closes the connection.
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {				
				throw e;
			} catch (IOException e) {				
				throw e;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//ALWAYS notify caller of collected emails for provided key 
			if (downloadCallback != null) {						
				downloadCallback.done(emails);
			}	
		}		
	}

	public static String canonicalEmail(String userOrEmail) {
		int pos = userOrEmail.indexOf('@');
		if (pos < 0) {
			return null;
		} else {
			return canonicalEmail(userOrEmail.substring(0, pos), userOrEmail.substring(pos));
		}
	}

	public static String canonicalEmail(String userName, String host) {
		if (host.toLowerCase().startsWith("@gmail.")) {
			return canonicalGmailUserName(userName) + host;
		}
		return userName + host;
	}

	public static String canonicalGmailUserName(String userName) {
		userName = userName.replace(".", "");
		int pos = userName.indexOf('+');
		if (pos < 0) {
			return userName;
		} else {
			return userName.substring(0, pos);
		}
	}

}


