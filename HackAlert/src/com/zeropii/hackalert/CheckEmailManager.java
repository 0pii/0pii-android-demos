package com.zeropii.hackalert;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author flado
 *
 */
public class CheckEmailManager {
	

	private static final CheckEmailManager instance;
	    
    private final ExecutorService threadPool;
    
	static  {    
        instance = new CheckEmailManager();
    }
	
	private CheckEmailManager() {	    
        threadPool = Executors.newCachedThreadPool();    
	}
	
	public void execute(DownloadTask task) {
		threadPool.execute(task);			
	}
	
	public static CheckEmailManager getInstance() {
		return instance;
	}
	
	public void shutdown() {
		threadPool.shutdown();
		
	}
	
	public boolean isTerminated() {
		return threadPool.isTerminated();
	}
	
	public void awaitTermination(long seconds) {
		try {
			threadPool.awaitTermination(seconds, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
