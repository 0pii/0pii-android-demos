package com.zeropii.hackalert;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

/**
 * CheckBox that stores email
 * 
 * @author flado
 *
 */
public class ContactCheckBox extends CheckBox {
	private String email;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ContactCheckBox(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ContactCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ContactCheckBox(Context context) {
		super(context);
	}
}