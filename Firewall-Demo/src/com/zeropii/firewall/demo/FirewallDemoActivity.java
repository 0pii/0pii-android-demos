/**
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * For more information, please refer to <http://unlicense.org/>
 */

package com.zeropii.firewall.demo;


import com.zeropii.firewall.interfaces.IFirewallService;
import com.zeropii.firewall.interfaces.IHttpListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.zeropii.firewall.lib.FirewallConnection;
import com.zeropii.firewall.lib.FirewallConnectionListener;
import com.zeropii.firewall.lib.FirewallConnector;

public class FirewallDemoActivity extends Activity implements FirewallConnectionListener {
	private static final String TAG = "FirewallDemoActivity";

	private FirewallConnection firewallConnection;
	
	Button buttonConnect;
  Button buttonGo;
  EditText editTextUrl;
  WebView webViewResult;
  
  FirewallConnector firewallConnector;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		firewallConnector = new FirewallConnector(this, this, true, 1);
		firewallConnector.connect();
		
		super.setContentView(R.layout.main);
		
		buttonConnect = (Button) super.findViewById(R.id.buttonConnect);
		buttonGo = (Button) super.findViewById(R.id.buttonGo);
		editTextUrl = (EditText) super.findViewById(R.id.editTextUrl);
		webViewResult = (WebView) super.findViewById(R.id.webViewResult);
		
		webViewResult.setWebViewClient(new WebViewClient() {
	    @Override
	    public void onPageStarted(WebView view, String url, Bitmap favicon) {
	      if (!url.startsWith("data:text/html")) {
	        view.stopLoading();
	        requestFirewallUrl(url);
	      }
	    }
		});
		
		buttonConnect.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        firewallConnector.connect();
      }
    });
		
		buttonGo.setEnabled(false);
    buttonGo.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        String url = editTextUrl.getText().toString();
        runOnUiThread(new Runnable() {
          
          @Override
          public void run() {
            webViewResult.loadData("<html><body>Waiting for Internet Firewall Service to respond...</body></html>", "text/html; charset=UTF-8", null);
          }
        });
        
        requestFirewallUrl(url);
      }
    });
	}
	
	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy()'ed");

    firewallConnector.dissconnect();

		super.onDestroy();
	}
	
	@Override
	protected void onResume() {
		Log.d(TAG, "onResume()'ed");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause()'ed");
		super.onPause();
	}

  @Override
  public void connected(FirewallConnection connection) {
    Log.d(TAG, "firewall connected");
    this.firewallConnection = connection;
    this.buttonConnect.setEnabled(false);
    this.buttonGo.setEnabled(true);
  }

  @Override
  public void disconnected(FirewallConnection connection) {
    Log.d(TAG, "firewall dissconnected");
    this.firewallConnection = null;
    this.buttonConnect.setEnabled(true);
    this.buttonGo.setEnabled(false);
  }

  @Override
  public void haveVersion(int version) {
    // NOP
  }
  
  private void requestFirewallUrl(String url) {
    try {
      firewallConnection.httpGet(new IHttpListener.Stub() {
        StringBuffer sb = new StringBuffer();

        @Override
        public void data(String chars) throws RemoteException {
          if (sb != null) {
            sb.append(chars);
          }
        }

        @Override
        public void done() throws RemoteException {
          if (sb == null) {
            return;
          }
          final String content = sb.toString(); 
          sb = null;
          if (content.length() > 0) {
            runOnUiThread(new Runnable() {
              
              @Override
              public void run() {
                webViewResult.loadData(content, "text/html; charset=UTF-8", null);
              }
            });                
          } else {
            runOnUiThread(new Runnable() {
              
              @Override
              public void run() {
                webViewResult.loadData("<html><body>Internet Firewall Service empty response. The url might be blacklisted, or the user denied access to the url.</body></html>", "text/html; charset=UTF-8", null);                
              }
            });                
          }
        }

        @Override
        public void error(final String err) throws RemoteException {
          if (sb == null) {
            return;
          }        
          runOnUiThread(new Runnable() {
            
            @Override
            public void run() {
              webViewResult.loadData("<html><body>Internet Firewall Service error: " + err + "</body></html>", "text/html; charset=UTF-8", null);                }
          });  
          sb = null;
        }

        @Override
        public void flush(String checkpoint) throws RemoteException {
          // NOP - not sinking
        }

        @Override
        public void reset() throws RemoteException {
          // NOP - not sinking
        }
      }, url, "", "User asked to view this page.");
    } catch (final RemoteException e) {
      runOnUiThread(new Runnable() {
        
        @Override
        public void run() {
          String msg = e.getMessage();
          if (msg == null) {
            msg = "Unknown";
          }
          webViewResult.loadData("<html><body>Internet Firewall Service Remote Exception: " + msg + "</body></html>", "text/html; charset=UTF-8", null);                }
      });  
    }
  }
  
}
