/**
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * For more information, please refer to <http://unlicense.org/>
 */

package com.zeropii.affiliate.demo;


import com.zeropii.firewall.interfaces.IHttpListener;
import com.zeropii.affiliate.demo.R;
import com.zeropii.paperss.lib.AdView;
import com.zeropii.paperss.lib.IServiceAvailabilityNotifier;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class PAPeRSSAffiliateDemoActivity extends Activity {
	private static final String TAG = "PAPeRSSAffiliateDemoActivity";


  CheckBox checkBoxStatus;
  CheckBox checkBoxAd;
  
  AdView adViewTop;
//  AdView adView;
  
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
    //this.requestWindowFeature(Window.FEATURE_NO_TITLE);

    super.setContentView(R.layout.main);

    checkBoxStatus = (CheckBox) super.findViewById(R.id.checkBoxStatus);
    checkBoxAd = (CheckBox) super.findViewById(R.id.checkBoxAd);
    
    adViewTop = (AdView) super.findViewById(R.id.adViewTop);
    //adView = (AdView) super.findViewById(R.id.adView);
    adViewTop.onCreate();
    
    checkBoxStatus.setOnCheckedChangeListener(new OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //Remove notification bar
        if (isChecked) {
          PAPeRSSAffiliateDemoActivity.this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
          PAPeRSSAffiliateDemoActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
      }
    });
    
    checkBoxAd.setOnCheckedChangeListener(new OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        adViewTop.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        //adView.setVisibility(isChecked ? View.GONE : View.GONE);
      }
    });
	}
	
	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy()'ed");
    adViewTop.onDestroy();
		super.onDestroy();
	}
	
	@Override
	protected void onResume() {
		Log.d(TAG, "onResume()'ed");
		super.onResume();
    adViewTop.onResume();
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause()'ed");
		super.onPause();
    adViewTop.onPause();
	}

  @Override
  protected void onStart() {
    Log.d(TAG, "onPause()'ed");
    super.onStart();
    adViewTop.onStart();
  }
}
